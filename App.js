import React from 'react';
import { StyleSheet, Text, Button, View, TextInput } from 'react-native';
import Serial from './Serial.js'

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      mnemonic: '',
      mainPrivateKey: ''
    };

    this._generateMnemonic = this._generateMnemonic.bind(this);
  }

  _generateMnemonic() {
    // generate mnemonic

    this.setState({
        mnemonic: 'dajfljfl',
      });
  }

  _writeMnemonic(mnemonic) {
    Serial.write(mnemonic)
  }

  _getPrivateKeyByPaths() {

  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Mnemonic: {this.state.mnemonic}</Text>
        <Button onPress={this._generateMnemonic} title="Generate New Mnemonic"/>
        <Button onPress={this._writeMnemonic(this.state.mnemonic)} title="Write Mnemonic"/>
        <TextInput onChangeText={(text) => this.setState({paths:text})} />
        <Button onPress={this._getPrivateKeyByPaths} title="Get Private Key"/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
