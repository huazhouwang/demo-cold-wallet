package com.android;

import android.content.Context;
import android.hardware.SerialManager;
import android.hardware.SerialPort;
import android.text.TextUtils;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.IOException;
import java.nio.ByteBuffer;

public class SerialModule extends ReactContextBaseJavaModule {
    private SerialManager mSerialManager;

    public SerialModule(ReactApplicationContext reactContext) {
        super(reactContext);

        mSerialManager = (SerialManager) reactContext.getSystemService(Context.SERIAL_SERVICE);
    }

    @Override
    public String getName() {
        return "Serial";
    }

    @ReactMethod
    public void write(String text, Promise promise) {
        SerialPort mSerialPort;
        String[] ports = mSerialManager.getSerialPorts();

        if (!TextUtils.isEmpty(text) && ports != null && ports.length > 0) {
            try {
                mSerialPort = mSerialManager.openSerialPort(ports[0], 57600);

                if (mSerialPort != null) {
                    new WriteThread(mSerialPort, text, promise).start();
                }
            } catch (IOException e) {
                e.printStackTrace();

                promise.reject(e);
            }
        } else {
            promise.reject("1", "");
        }
    }

    private static final class WriteThread extends Thread {
        private final SerialPort serialPort;
        private final String text;
        private final Promise promise;

        private WriteThread(SerialPort serialPort, String text, Promise promise) {
            this.serialPort = serialPort;
            this.text = text;
            this.promise = promise;
        }

        @Override
        public void run() {
            ByteBuffer mOutputBuffer = ByteBuffer.allocateDirect(2048);

            try {
                byte[] bytes = hexStringToByteArray(text);
                System.out.format("send  data  to serial is ====\n");

                for (byte aByte : bytes) {
                    System.out.format("%02x", aByte);
                }

                System.out.format("\n");
                System.out.format("send data end ====\n");
                //	byte[] bytes = text.getBytes();
                mOutputBuffer.clear();
                mOutputBuffer.put(bytes);
                serialPort.write(mOutputBuffer, bytes.length);
                promise.resolve("Success");
            } catch (IOException e) {
            }
        }
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        try {
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i+1), 16));
            }
        } catch (Exception e) {
            Log.d("", "Argument(s) for hexStringToByteArray(String s)"+ "was not a hex string");
        }
        return data;
    }
}
